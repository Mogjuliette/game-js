import { Chapter } from "./entities";
import { Character } from "./entities"
import { Ability } from "./entities";
import * as images from '../public/image/*.png' 


console.log("jeu Paper Forest")

let aboutTrancher = "Une attaque rapide de 3"
let aboutGriffe = "Une petite attaque de 2"
let aboutProteger = "Se défendre contre la prochaine attaque de 2"
let aboutAttaqueChargee = "Une grosse attaque de 6 qui demande un tour de préparation"
let aboutEsquive = "Tenter d'esquiver la prochaine attaque"

let noCharacter:Character = {
    name:'',
    imgLeft:'',
    imgRight:"",
    abilities:[],
    pv:10,
    pvMax:10,
    mainFighter:false,
    armor:0
}

let ecureuil:Character = {
    name:'Ecureuil',
    imgLeft:images.ecureuil_left,
    imgRight:images.ecureuil_right,
    abilities:[
        {
            name:'Esquive',
            value:0.6,
            about:aboutEsquive,
            on:false
        },
        {
            name:'Griffe',
            value:2,
            about:aboutGriffe
        }
    ],
    pv:8,
    pvMax:8,
    mainFighter:false,
    armor:0
}

let hibou:Character = {
    name:'Hibou',
    imgLeft:images.hibou_left,
    imgRight:images.hibou_right,
    abilities:[
            {
                name:'Griffe',
                value:2,
                about:aboutGriffe
            },
            
            {
                name:'Attaque chargée',
                value:6,
                about:aboutAttaqueChargee,
                on:false
            },
            {
                name:'Esquive',
                value:0.5,
                about:aboutEsquive,
                on:false
            }
            ],
    pv:10,
    pvMax:10,
    mainFighter:false,
    armor:0
}

let mamanRenard:Character = {
    name : 'Renarde',
    imgLeft : images.maman_renard_left,
    imgRight : images.maman_renard_right,
    abilities:[
        {
            name:'Trancher',
            value:3,
            about: aboutTrancher
        },
        
        {
            name:'Protéger',
            value:2,
            about:aboutProteger,
            on:false
        },
        {
            name:'Esquive',
            value:0.4,
            about:aboutEsquive,
            on:false
        }
        ],
    pv:12,
    pvMax:12,
    mainFighter:false,
    armor:0
}

let ours:Character = {
    name:'Ours',
    imgLeft:images.ours_left,
    imgRight:images.ours_right,
    abilities:[
        {
            name:'Trancher',
            value:3,
            about: aboutTrancher
        },
        
        {
            name:'Attaque chargée',
            value:6,
            about:aboutAttaqueChargee,
            on:false
        }, 
        {
            name:'Protéger',
            value:2,
            about:aboutProteger,
            on:false
        }
        
    ],
    pv:10,
    pvMax:10,
    mainFighter:false,
    armor:0
}

let petitRenard:Character = {
    name:'petit Renard',
    imgLeft:images.petit_renard_2_left,
    imgRight:images.petit_renard_2_right,
    abilities:[],
    pv:10,
    pvMax:10,
    mainFighter:false,
    armor:0
}


let tsuru:Character = {
    name : 'Tsuru',
    imgLeft : images.rose_cropped_left,
    imgRight : images.rose_cropped_right,
    abilities:[],
    pv:10,
    pvMax:10,
    mainFighter:false,
    armor:0
}

let tabAllChapters:Chapter[][]= [
    [
        {
            characterLeft : tsuru,
            characterRight : ecureuil,
            talkingCharacter: noCharacter,
            dialogues : [
            "Dans la forêt du Pli, que l'on appelle aussi Oritatami no mori, vivent des Origamins, des créatures de papiers ayant pris vie. En temps normal, des mikos, prêtresses se consacrant à l'art de l'origami veillent sur eux. La princesse Tsuru règne sur la forêt du Pli et tout ses habitants.",
            "Mais un jour la princesse du Thé hivernal jeta un sortilège sur la forêt du pli. Les Origamins, influencés par son emprise maléfique devinrent aggressifs. Les mikos fuirent, et de la princesse Tsuru on n'eut plus de nouvelles.",
            "Une Renarde origamine était en voyage lorsque le sortilège fut lancé. Lorsqu'elle revint, elle se mit en tête de combattre tout les Origamins maléfique jusqu'à retrouver son petit"
        ],},
    
        {
            characterLeft : mamanRenard,
            characterRight : noCharacter,
            talkingCharacter: mamanRenard,
            dialogues : [
                "Ils sont tous devenus fous ! Je ne peux rien y faire ! Il ne reste plus qu'à me battre jusqu'à le retrouver !",
                "Craignez-moi !"
            ],
        },

        {   characterLeft : mamanRenard,
            characterRight : hibou,
            talkingCharacter : mamanRenard,
            dialogues : [
            "Hibou ! Mon ami ! Tu ne me reconnais donc pas ?",
            ]
        },

        {   characterLeft : mamanRenard,
            characterRight : hibou,
            talkingCharacter : hibou,
            dialogues : [
            "AAARRRRAAAGGHH",
            ]
        }
    
    ],[
        {   characterLeft : mamanRenard,
            characterRight : hibou,
            talkingCharacter : hibou,
            dialogues : [
            "Je suis désolée de t'avoir attaquée Renarde, je ne sais pas ce qui m'a pris",
            "J'espère que tu me pardonneras !"
            ]
        },
    
        {
            characterLeft : mamanRenard,
            characterRight : hibou,
            talkingCharacter : mamanRenard,
            dialogues : [
                "Ce n'est pas ta faute Hibou, tu étais sous l'influence du sortilège de la Princesse du thé hivernal.",
                "Je cherche mon petit, je vais continuer mon chemin dans la forêt jusqu'à le retrouver"
            ],
        },
    
        {
            characterLeft : mamanRenard,
            characterRight : hibou,
            talkingCharacter : hibou,
            dialogues : [
                "Attends, je vais t'accompagner ! A deux nous seront plus forts !",
                "Attention, voilà Ours ! Il va nous attaquer !"
            ],
        },

        {
            characterLeft : mamanRenard,
            characterRight : ours,
            talkingCharacter : ours,
            dialogues : [
                "Je vais vous déchiqueter ! !"
            ],
        }
    ],[
        {
            characterLeft : mamanRenard,
            characterRight : ours,
            talkingCharacter : ours,
            dialogues : [
                "Oh, pardon Renarde ! Pardon Hibou ! Vous allez bien ?",
                "Je ne vous ai pas fait trop de dégâts ?"
            ],
        },
        {
            characterLeft : mamanRenard,
            characterRight : ours,
            talkingCharacter : mamanRenard,
            dialogues : [
                "Oui ne t'inquiète pas !",
                "Tu veux nous accompagner ? Je cherche mon renardeau !"
            ],
        },
        {
            characterLeft : hibou,
            characterRight : ecureuil,
            talkingCharacter : hibou,
            dialogues : [
                "Attention, Ecureuil se jette sur nous !"
            ],
        },
    ],
    [
        {
            characterLeft : hibou,
            characterRight : ecureuil,
            talkingCharacter : ecureuil,
            dialogues : [
                "Oh ! Je suis à nouveau moi-même ! Merci beaucoup ! ",
                "J'ai vu Renardeau caché derrière ce bosquet, venez, je vais vous mener à lui",
                "Suivez-moi !"
            ],
        },
        {
            characterLeft : mamanRenard,
            characterRight : petitRenard,
            talkingCharacter : petitRenard,
            dialogues : [
                "Maman ! Maman !!!",
                "Mon bébé !"
            ],
        }
        ,
        {
            characterLeft : mamanRenard,
            characterRight : petitRenard,
            talkingCharacter : noCharacter,
            dialogues : [
                "Et c'est ainsi que l'aventure de Renarde prit fin. Mais peut-être affrontera-t-elle d'autres ennemis dans une prochaine aventure !",
            ],
        }
    ]
]

let tabChoices:Character[][] = [
    [mamanRenard], 
    [ mamanRenard,hibou],
    [mamanRenard,hibou,ours]
]

let tabOpponents:Character[] = [hibou,ours,ecureuil]

let indexDialogue=0;
let indexTab=0;
let indexOfChoice = 0
let interfaceBox = document.querySelector<HTMLDivElement>('.interface-box')
let characterLeft = document.querySelector<HTMLImageElement>('.character-left')
let characterRight = document.querySelector<HTMLImageElement>('.character-right')
const rightContainer = document.querySelector<HTMLDivElement>('.right-container')
const leftContainer = document.querySelector<HTMLDivElement>('.left-container')
let mainWindow = document.querySelector<HTMLDivElement>('.main-window')
let innerMenu = document.querySelector<HTMLDivElement>('.inner-menu')
const menu = document.querySelector<HTMLDivElement>('.menu')
let dodged = false
let lastFighterAction:Ability
let lastOpponentAction:Ability

dialogueAndchoices(tabAllChapters[indexOfChoice]);

/**
 * Tour de bataille du combattant contrôlé par la joueuse
 * @param fighter Le combattant
 * @param actions Le tableau d'abilities du combattant
 * @param opponent L'adversaire
 */
function battleBegin(fighter:Character,actions:Ability[],opponent:Character){
    indexOfChoice++;
    fighter.mainFighter=true;
    if(characterLeft){
        characterLeft.src=fighter.imgLeft
    }
    if(characterRight){
        characterRight.src=opponent.imgRight
    }
    if(leftContainer){
        leftContainer.innerHTML=`<h4>${fighter.name}</h4><p>Points de vie : <span class="pv-number-left">${fighter.pv}/${fighter.pvMax}</span></p>
        <div class="progress margin-bottom">
          <div class="bar warning w-100 bar-left"></div>
        </div>
        <div class="ability-property">
        </div>`
    }
    if(rightContainer){
        rightContainer.innerHTML=`<h4>${opponent.name}</h4><p>Points de vie : <span class="pv-number-right">${opponent.pv}/${opponent.pvMax}</span></p>
        <div class="progress margin-bottom">
            <div class="bar warning w-100 bar-right">
            </div>
        </div>
        <div class="ability-property-opponent">
        </div>`
    }
    if(interfaceBox){
        interfaceBox.classList.add('row');
        interfaceBox.innerHTML=`<div class="div-abilities colonne col-4"></div>
        <div class="middle col-8 text-center">Bataille !</div>`;
    }
    showPV(fighter,"left")
    showPV(opponent,"right")
    return fighterBattle(fighter,actions,opponent)
}

/**
 * Lance le combat
 * @param fighter combattant principal
 * @param actions abilities du combattant principal
 * @param opponent opposant du combat
 */
function fighterBattle(fighter:Character,actions:Ability[],opponent:Character){
    if(leftContainer && rightContainer){
        leftContainer.style.opacity="1"
        rightContainer.style.opacity="1"
    }
    let divAbilities = document.querySelector<HTMLDivElement>('.div-abilities') 
    let abilityProperty = document.querySelector<HTMLDivElement>('.ability-property')
    for (let i = 0; i < actions.length; i++) {
            let divAbility = document.createElement('div')
            if(divAbilities)
            divAbilities.appendChild(divAbility)
            let nameAbility = document.createElement('h5')
            if(divAbility)
            divAbility.appendChild(nameAbility)
            nameAbility.innerHTML=actions[i].name
            divAbility?.addEventListener('mouseover',()=>{
                if(abilityProperty){
                    abilityProperty.innerHTML= `<p>${actions[i].name}</p><p>${actions[i].about}</p>`
                }
            }) 
            divAbility?.addEventListener('mouseout',()=>{
                if(abilityProperty){
                    abilityProperty.innerHTML= ""
                }
            })
            divAbility?.addEventListener('click',()=>{
                if(divAbilities){
                    divAbilities.innerHTML="";
                }
                battleAction(fighter,actions[i],opponent,"right")  
            })
    } 
}

/**
 * Ce qu'il se passe une fois que l'action a été choisie pendant la bataille
 * @param action L'action choisie par le combattant
 * @param opponent L'adversaire
 * @returns 
 */
function battleAction(fighter:Character,action:Ability,opponent:Character,opposantSide:string){
    let middleContent = document.querySelector<HTMLDivElement>('.middle')
    if(lastFighterAction != undefined && lastOpponentAction != undefined){
    }
    let abilityProperty = document.querySelector<HTMLDivElement>(".ability-property")
    if(abilityProperty){
        abilityProperty.innerHTML=""
    }
    let abilityPropertyOpponent = document.querySelector<HTMLDivElement>('.ability-property-opponent')
        if(abilityPropertyOpponent){
            abilityPropertyOpponent.innerHTML= ""
        }
    
    let damage = action.value - opponent.armor
    if (damage > opponent.pv){
        damage = opponent.pv
    }
    if(action.name == "Attaque chargée"){
        if(action.on == false){
            action.on = true;
            dodged = false
            if(middleContent)
                middleContent.innerHTML=`${fighter.name} charge l'attaque`
            
        }else {
            action.on = false
            if(dodged == true){
                if(middleContent)
                middleContent.innerHTML=`${opponent.name} esquive l'attaque`
                dodged = false
            } else {
               opponent.pv-= (action.value-opponent.armor)
               if (opponent.pv < 0)
               opponent.pv= 0
                if(middleContent)
                middleContent.innerHTML=`${opponent.name} perd ${damage} pv` 
            }
        }
    }
    if(dodged == true){
            if(middleContent)
            middleContent.innerHTML=`${opponent.name} esquive l'attaque`
            dodged = false
    } else {
        if(action.name == "Trancher"){
            opponent.pv-= damage
            if(middleContent)
            middleContent.innerHTML=`${opponent.name} perd ${damage} pv`
        }

        if(action.name == "Griffe"){
            opponent.pv-= damage
            if(middleContent)
            middleContent.innerHTML=`${opponent.name} perd ${damage} pv`
        }
    }
    if(action.name == "Protéger"){
        fighter.armor =  action.value
        if(middleContent)
        middleContent.innerHTML=`Protégé.e de ${action.value} pour la prochaine attaque !`
        dodged = false
    }
    if(action.name==="Esquive"){
        if(Math.random()<action.value){
            dodged=true
            if(middleContent)
            middleContent.innerHTML="Esquive de la prochaine attaque réussie !"
        } else {
            dodged = false
            if(middleContent)
            middleContent.innerHTML="Esquive de la prochaine attaque ratée !"
        }
    }
const pvNumber = document.querySelector<HTMLSpanElement>(`.pv-number-${opposantSide}`)
if(pvNumber){
    pvNumber.innerHTML=`${opponent.pv}/${opponent.pvMax}`
} 
showPV(opponent,opposantSide)
opponent.armor=0 
if(fighter.mainFighter == true){
    lastFighterAction = action   
} else {
    lastOpponentAction = action
}
setTimeout(()=>{
    if(opponent.pv <= 0){
        if(fighter.mainFighter == true){
            if(middleContent)
            middleContent.innerHTML="Victoire ! Fin du combat !"
            setTimeout(()=>{
                fighter.pv = fighter.pvMax
                opponent.pv = opponent.pvMax
                dialogueAndchoices(tabAllChapters[indexOfChoice]);     
            }, 1000)
            return
        } else {
            if(middleContent)
            middleContent.innerHTML="Défaite ! Vous avez perdu !"
            if(interfaceBox){
                interfaceBox.innerHTML=`<button class="start-again">Recommencer le combat ?</button>`
                interfaceBox.classList.add('centered')
            }
            let startAgainBtn = document.querySelector<HTMLButtonElement>('.start-again')
            startAgainBtn?.addEventListener('click',() => {
                opponent.pv = opponent.pvMax;
                fighter.pv = fighter.pvMax;
                if(interfaceBox){
                    interfaceBox.classList.remove('centered')
                }
                battleBegin(opponent,opponent.abilities,fighter);
            })
            return
        }
        
    }
    if(opponent.mainFighter == false){
         opponentBattle(opponent,fighter)
         return
    }else {
        fighterBattle(opponent,opponent.abilities,fighter)
        return 
    }
},2000)
}

/**
 * Update l'affichage des pv d'un personnage
 * @param character le personnage en question
 * @param characterSide le côté de l'écran du personnage
 */
function showPV(character:Character,characterSide:string){
    const pvNumber = document.querySelector<HTMLSpanElement>(`.pv-number-${characterSide}`)
    if(pvNumber){
        pvNumber.innerHTML=`${character.pv}/${character.pvMax}`
    } 
    const bar = document.querySelector<HTMLDivElement>(`.bar-${characterSide}`)
    if(bar){
        for (let i = bar.classList.length - 1; i >= 0; i--) {
            const className = bar.classList[i];
            if (className.startsWith('w-')) {
                bar.classList.remove(className);
            }
        }
        let barPercent = Math.round(character.pv/character.pvMax*100)
        bar.classList.add(`w-${barPercent}`)
    }   
}

/**
 * Le tour du combattant
 * @param opponent L'opposant du combat
 * @param figther Le combattant principal
 */
function opponentBattle(opponent:Character,fighter:Character){
    let middleContent = document.querySelector<HTMLDivElement>('.middle')

    setTimeout(()=>{
        if(middleContent)
        middleContent.innerHTML=`C'est au tour de ${opponent.name} !` 
    },500)
    let choosenAction:Ability = opponent.abilities[Math.floor(Math.random() * opponent.abilities.length)];
    setTimeout(()=>{
        
        if(middleContent){
            middleContent.innerHTML=`${opponent.name} utilise ${choosenAction.name}!`
        let abilityPropertyOpponent = document.querySelector<HTMLDivElement>('.ability-property-opponent')
        if(abilityPropertyOpponent){
            abilityPropertyOpponent.innerHTML= `<p>${choosenAction.name}</p><p>${choosenAction.about}</p>`
        }
    }
    },2000)
    setTimeout(() =>{
        battleAction(opponent,choosenAction,fighter,"left")
    },4000)
    setTimeout(() =>{
        if(middleContent){
            middleContent.innerHTML=`C'est au tour de ${fighter.name} !`
        }
    },6000)
}

/**
 * Permet de lancer et afficher les dialogues entre personnages
 * @param currentTabChapter Tableaux de dialogues
 */
function dialogueAndchoices(currentTabChapter:Chapter[]) {
    if(interfaceBox){
        interfaceBox.classList.remove("row");
    }
    if(leftContainer&&rightContainer){
        leftContainer.style.opacity="0"
        rightContainer.style.opacity="0"
    }
    indexDialogue=0;
    indexTab=0;
    showDialogue(currentTabChapter);
    interfaceBox?.addEventListener('click', function loopDialogues() {
        if (indexTab < currentTabChapter.length) {
            if (indexDialogue + 1 < currentTabChapter[indexTab].dialogues.length) {
                indexDialogue++;
                showDialogue(currentTabChapter);
                if (indexDialogue + 1 == currentTabChapter[indexTab].dialogues.length) {
                    indexTab++;
                    indexDialogue = -1;
                }
            }
        } else {  
            if(interfaceBox){
                interfaceBox.removeEventListener('click', loopDialogues) 
            }
            if(indexOfChoice == 0){
                battleBegin(mamanRenard,mamanRenard.abilities,hibou);
            } else if(indexOfChoice < 3) {
                showChoice();
            } else {
                if(interfaceBox){
                    if(interfaceBox){
                        interfaceBox.classList.add('centered')
                    }
                    interfaceBox.innerHTML="Bravo ! Fin du jeu !"
                }
            }
            return;
        }
    });
}

/**
 * Montre les dialogues et les personnages de ces dialogues
 * @param currentTabChapter Tableau des dialogue
 * @returns 
 */
function showDialogue(currentTabChapter:Chapter[]) {
    if(interfaceBox){
        interfaceBox.innerHTML=`<h4 class="character-heading"></h4>
        <p class="dialogue-para"></p>`
    }
    let dialoguePara = document.querySelector<HTMLParagraphElement>('.dialogue-para')
    let characterHeading = document.querySelector<HTMLHeadingElement>('.character-heading')
    if (dialoguePara) {
        dialoguePara.innerHTML = `${currentTabChapter[indexTab].dialogues[indexDialogue]} <span class="dots border-bottom">.   .   .</span>`;
    }
    if(characterHeading){
        characterHeading.innerHTML = `${currentTabChapter[indexTab].talkingCharacter.name}`
    }
    if(characterLeft){
        characterLeft.src=currentTabChapter[indexTab].characterLeft.imgLeft
    }
    if(characterRight){
        characterRight.src=currentTabChapter[indexTab].characterRight.imgRight
    }
    return
}

/**
 * Offre un choix dans la narration
 * @param tabOfChoices Tableau des différents choix
 * @returns 
 */
function showChoice(){
    if(mainWindow){
        mainWindow.style.display = "none";
    }
    if(interfaceBox){
        interfaceBox.innerHTML = "";
    }
    if(menu && innerMenu){
        menu.style.display = "flex";
        const choiceQuestion = document.createElement('h5');
        choiceQuestion.classList.add('margin-none');
        choiceQuestion.innerHTML=`<h3 class='text-center'>Choisissez votre combattant : </h3>`;
        innerMenu.appendChild(choiceQuestion);
        let divFighters = document.createElement('div');
        divFighters.classList.add('row','flex-spaces','tabs')
        innerMenu.appendChild(divFighters)
        for (const [index, fighter] of tabChoices[indexOfChoice].entries()) {
            let index1 = index + 1;
            if(index == 0){
                divFighters.innerHTML+=`
                <input id="tab${index1}" type="radio" name="tabs" checked>
                <label for="tab${index1}">${fighter.name}</label>`;
            } else {
                divFighters.innerHTML+=`
                <input id="tab${index1}" type="radio" name="tabs">
                <label for="tab${index1}">${fighter.name}</label>`;
            }
            }
            
        for (const [index, fighter] of tabChoices[indexOfChoice].entries()) {
            let index1 = index + 1;
            divFighters.innerHTML+=`
            <div class="content margin" id="content${index1}">
            <h4 class="margin-left-none">${fighter.name}</h4>
            <p><span class="pv-title">PV : </span>${fighter.pv}/${fighter.pvMax}</p>
            <h5>Capacités : </h5>
            </div>`;
            let divContent = document.querySelector(`#content${index1}`)
            for (const ability of fighter.abilities) {
                if(divContent)
                divContent.innerHTML+= `<p>${ability.name} : ${ability.about}</p>`         
            } 
            if(divContent)
                divContent.innerHTML+= `<button class="margin-top-large" id="btn${index}">Choisir ${fighter.name}</button>`  ;
        }
    }    
    let btn0 = document.querySelector<HTMLButtonElement>(`#btn0`) 
    let btn1 = document.querySelector<HTMLButtonElement>(`#btn1`)
    let btn2 = document.querySelector<HTMLButtonElement>(`#btn2`)  
    
    /**
     * Permet de gérer l'addEventListener des boutons pour choisir le combattant
     * @param button Le bouton sélectionné
     * @param index L'index dans le tableau de choix de combattant de notre combattant
     */
    function choosingFighterBtn(button:HTMLButtonElement,index:number){
        if(button){
            button.addEventListener('click', ()=>{
            if(menu && innerMenu){
                innerMenu.innerHTML=""
                menu.style.display="none";
            }
            if(mainWindow){
               mainWindow.style.display="block";
            }
            battleBegin(tabChoices[indexOfChoice][index],tabChoices[indexOfChoice][index].abilities,tabOpponents[indexOfChoice])
            })  
        }
    }

    if(btn0)
    choosingFighterBtn(btn0,0)
    if(btn1)
    choosingFighterBtn(btn1,1)
    if(btn2)
    choosingFighterBtn(btn2,2) 
}
