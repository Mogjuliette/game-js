export interface Ability {
    name:string,
    value:number,
    about:string,
    on?:boolean,
}

export interface Character {
        name:string,
        imgLeft:string,
        imgRight:string,
        abilities:Ability[],
        pv:number,
        pvMax:number,
        mainFighter:boolean,
        armor:number,
}

export interface Chapter {
    characterLeft:Character,
    characterRight:Character,
    talkingCharacter:Character
    dialogues:string[]
}