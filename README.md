# Projet Jeu JS

Paper Forest est un jeu de type Pokemon battle avec une composante narrative. Dans une forêt, des personnages origamis s'affrontent à tour de rôle dans du combat au tour par tour. On suit le personnage de la Renarde qui cherche à retrouver son enfant Renardeau après qu'un maléfice ait rendu les origamis aggressifs. Chaque personnage battu rejoint l'équipe de la Renarde, et il est possible de choisir quel personnage on souhaite jouer pour le prochain combat.

Le jeu est jouable en l'état, mais je l'ai créé dans l'idée qu'il puisse être facilement améliorable avec plus de temps de travail (plus de personnages, de combat, des musiques, des animations, des nouvelles capacités en combat.)

Le projet fait appel à plusieurs interfaces, manipule des tableaux, des objets, des tableaux d'objets contenant eux-même tableaux ou objets. La variable des points de vie des personnages peut-être modifiée en jeu, ainsi que des variables de statut (prochaine esquive réussie, armure mise, attaque chargée). La barre des points de vie des personnages est modifiée en jeu en fonction de l'état des données.

Le jeu permet aussi de manipuler le DOM avec des event (principalement du clic sur des div ou des boutons) et des systèmes d'affichage qui varient en fonction des situations (par exemple la description de l'action n'apparaît que lorsque l'on passe la souris au-dessus de l'action).

Les images png d'origami proviennent du site https://www.freepng.fr/ , la texture papier en fond d'écran vient de https://fr.freepik.com/photos-gratuite/fond-texture-papier-espace-design_3220799.htm#query=texture%20papier&position=2&from_view=keyword (j'essaie autant de possible d'utiliser des ressources libres de droit). Le dessin de forêt est une réalisation personnelle à la tablette graphique. J'ai construit le jeu autour du framework CSS papercss, que j'ai utilisé pour toute l'interface.

Les maquettes sur Figma sont disponibles avec le lien suivant : https://www.figma.com/file/jrSBNPVp1SkVhOKV9sx5JK/Jeu-Paper-Forest?node-id=0%3A1&t=Jaf1CkuHMmnpmjNz-1


## Consignes

Durée du projet : 2~ semaines

L'objectif de ce projet est de faire un petit jeu en Javascript sans framework.
Il va permettre d'utiliser le JS, l'algorithmie de base, la manipulation du DOM et la POO dans un contexte ludique... mais professionnel.

Le type et le thème du jeu sont libres (en sachant qu'il sera sur votre portfolio, donc pas n'importe quoi non plus).

## Organisation
* Choisir le thème et le type du jeu
* Faire 2-3 maquettes fonctionnelles de votre jeu
* Créer un board dans le dépôt gitlab du jeu dans lequel vous listerez toutes les fonctionnalités à coder
* Créer une milestone (un Sprint) par semaine et y indiquer l'état dans lequel vous souhaitez que le jeu soit au bout de ce sprint, assigner les fonctionnalités du board à la milestone
* Pour chaque fonction/méthode, je fais la JSDoc

## Aspects techniques obligatoires
* Utilisation des objets Javascript d'une manière ou d'une autre (genre une ou deux interfaces, ça serait bien)
* Avoir des données dans le JS qui seront modifiées en cours de jeu (barre de vie, score, progression, etc.), l'idée est de travailler la séparation des données et de l'affichage

## Notes
*  Les jeux en temps réels sont plus compliqués à gérer (genre un truc de plateforme, ou n'importe quel jeu où il se passe des choses sans action utilisateur·ice)
*  Les frameworks de jeu type phaserJS, bien qu'intéressant, ne sont pas autorisés car compétences trop spécifiques
*  Le jeu ne doit pas nécessairement être responsive (et c'est bien le seul projet qui ne le sera pas)

## Exemples de jeux
Pour les personnes qui n'ont pas d'idée voici quelques exemples de jeux faisables :
* Un Tamagochi
* Un Pokemon Battle simplifié (même principe que le tamagochi en fait, mais avec 2 persos)
* Un memory
* Un puissance 4
