// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

(function (modules, entry, mainEntry, parcelRequireName, globalName) {
  /* eslint-disable no-undef */
  var globalObject =
    typeof globalThis !== 'undefined'
      ? globalThis
      : typeof self !== 'undefined'
      ? self
      : typeof window !== 'undefined'
      ? window
      : typeof global !== 'undefined'
      ? global
      : {};
  /* eslint-enable no-undef */

  // Save the require from previous bundle to this closure if any
  var previousRequire =
    typeof globalObject[parcelRequireName] === 'function' &&
    globalObject[parcelRequireName];

  var cache = previousRequire.cache || {};
  // Do not use `require` to prevent Webpack from trying to bundle this call
  var nodeRequire =
    typeof module !== 'undefined' &&
    typeof module.require === 'function' &&
    module.require.bind(module);

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire =
          typeof globalObject[parcelRequireName] === 'function' &&
          globalObject[parcelRequireName];
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error("Cannot find module '" + name + "'");
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = (cache[name] = new newRequire.Module(name));

      modules[name][0].call(
        module.exports,
        localRequire,
        module,
        module.exports,
        this
      );
    }

    return cache[name].exports;

    function localRequire(x) {
      var res = localRequire.resolve(x);
      return res === false ? {} : newRequire(res);
    }

    function resolve(x) {
      var id = modules[name][1][x];
      return id != null ? id : x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [
      function (require, module) {
        module.exports = exports;
      },
      {},
    ];
  };

  Object.defineProperty(newRequire, 'root', {
    get: function () {
      return globalObject[parcelRequireName];
    },
  });

  globalObject[parcelRequireName] = newRequire;

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (mainEntry) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(mainEntry);

    // CommonJS
    if (typeof exports === 'object' && typeof module !== 'undefined') {
      module.exports = mainExports;

      // RequireJS
    } else if (typeof define === 'function' && define.amd) {
      define(function () {
        return mainExports;
      });

      // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }
})({"iJYvl":[function(require,module,exports) {
var global = arguments[3];
var HMR_HOST = null;
var HMR_PORT = null;
var HMR_SECURE = false;
var HMR_ENV_HASH = "d6ea1d42532a7575";
module.bundle.HMR_BUNDLE_ID = "5c1b77e3b71e74eb";
"use strict";
/* global HMR_HOST, HMR_PORT, HMR_ENV_HASH, HMR_SECURE, chrome, browser, globalThis, __parcel__import__, __parcel__importScripts__, ServiceWorkerGlobalScope */ /*::
import type {
  HMRAsset,
  HMRMessage,
} from '@parcel/reporter-dev-server/src/HMRServer.js';
interface ParcelRequire {
  (string): mixed;
  cache: {|[string]: ParcelModule|};
  hotData: mixed;
  Module: any;
  parent: ?ParcelRequire;
  isParcelRequire: true;
  modules: {|[string]: [Function, {|[string]: string|}]|};
  HMR_BUNDLE_ID: string;
  root: ParcelRequire;
}
interface ParcelModule {
  hot: {|
    data: mixed,
    accept(cb: (Function) => void): void,
    dispose(cb: (mixed) => void): void,
    // accept(deps: Array<string> | string, cb: (Function) => void): void,
    // decline(): void,
    _acceptCallbacks: Array<(Function) => void>,
    _disposeCallbacks: Array<(mixed) => void>,
  |};
}
interface ExtensionContext {
  runtime: {|
    reload(): void,
    getURL(url: string): string;
    getManifest(): {manifest_version: number, ...};
  |};
}
declare var module: {bundle: ParcelRequire, ...};
declare var HMR_HOST: string;
declare var HMR_PORT: string;
declare var HMR_ENV_HASH: string;
declare var HMR_SECURE: boolean;
declare var chrome: ExtensionContext;
declare var browser: ExtensionContext;
declare var __parcel__import__: (string) => Promise<void>;
declare var __parcel__importScripts__: (string) => Promise<void>;
declare var globalThis: typeof self;
declare var ServiceWorkerGlobalScope: Object;
*/ var OVERLAY_ID = "__parcel__error__overlay__";
var OldModule = module.bundle.Module;
function Module(moduleName) {
    OldModule.call(this, moduleName);
    this.hot = {
        data: module.bundle.hotData,
        _acceptCallbacks: [],
        _disposeCallbacks: [],
        accept: function(fn) {
            this._acceptCallbacks.push(fn || function() {});
        },
        dispose: function(fn) {
            this._disposeCallbacks.push(fn);
        }
    };
    module.bundle.hotData = undefined;
}
module.bundle.Module = Module;
var checkedAssets, acceptedAssets, assetsToAccept /*: Array<[ParcelRequire, string]> */ ;
function getHostname() {
    return HMR_HOST || (location.protocol.indexOf("http") === 0 ? location.hostname : "localhost");
}
function getPort() {
    return HMR_PORT || location.port;
} // eslint-disable-next-line no-redeclare
var parent = module.bundle.parent;
if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== "undefined") {
    var hostname = getHostname();
    var port = getPort();
    var protocol = HMR_SECURE || location.protocol == "https:" && !/localhost|127.0.0.1|0.0.0.0/.test(hostname) ? "wss" : "ws";
    var ws = new WebSocket(protocol + "://" + hostname + (port ? ":" + port : "") + "/"); // Web extension context
    var extCtx = typeof chrome === "undefined" ? typeof browser === "undefined" ? null : browser : chrome; // Safari doesn't support sourceURL in error stacks.
    // eval may also be disabled via CSP, so do a quick check.
    var supportsSourceURL = false;
    try {
        (0, eval)('throw new Error("test"); //# sourceURL=test.js');
    } catch (err) {
        supportsSourceURL = err.stack.includes("test.js");
    } // $FlowFixMe
    ws.onmessage = async function(event) {
        checkedAssets = {} /*: {|[string]: boolean|} */ ;
        acceptedAssets = {} /*: {|[string]: boolean|} */ ;
        assetsToAccept = [];
        var data = JSON.parse(event.data);
        if (data.type === "update") {
            // Remove error overlay if there is one
            if (typeof document !== "undefined") removeErrorOverlay();
            let assets = data.assets.filter((asset)=>asset.envHash === HMR_ENV_HASH); // Handle HMR Update
            let handled = assets.every((asset)=>{
                return asset.type === "css" || asset.type === "js" && hmrAcceptCheck(module.bundle.root, asset.id, asset.depsByBundle);
            });
            if (handled) {
                console.clear(); // Dispatch custom event so other runtimes (e.g React Refresh) are aware.
                if (typeof window !== "undefined" && typeof CustomEvent !== "undefined") window.dispatchEvent(new CustomEvent("parcelhmraccept"));
                await hmrApplyUpdates(assets);
                for(var i = 0; i < assetsToAccept.length; i++){
                    var id = assetsToAccept[i][1];
                    if (!acceptedAssets[id]) hmrAcceptRun(assetsToAccept[i][0], id);
                }
            } else fullReload();
        }
        if (data.type === "error") {
            // Log parcel errors to console
            for (let ansiDiagnostic of data.diagnostics.ansi){
                let stack = ansiDiagnostic.codeframe ? ansiDiagnostic.codeframe : ansiDiagnostic.stack;
                console.error("\uD83D\uDEA8 [parcel]: " + ansiDiagnostic.message + "\n" + stack + "\n\n" + ansiDiagnostic.hints.join("\n"));
            }
            if (typeof document !== "undefined") {
                // Render the fancy html overlay
                removeErrorOverlay();
                var overlay = createErrorOverlay(data.diagnostics.html); // $FlowFixMe
                document.body.appendChild(overlay);
            }
        }
    };
    ws.onerror = function(e) {
        console.error(e.message);
    };
    ws.onclose = function() {
        console.warn("[parcel] \uD83D\uDEA8 Connection to the HMR server was lost");
    };
}
function removeErrorOverlay() {
    var overlay = document.getElementById(OVERLAY_ID);
    if (overlay) {
        overlay.remove();
        console.log("[parcel] ✨ Error resolved");
    }
}
function createErrorOverlay(diagnostics) {
    var overlay = document.createElement("div");
    overlay.id = OVERLAY_ID;
    let errorHTML = '<div style="background: black; opacity: 0.85; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; font-family: Menlo, Consolas, monospace; z-index: 9999;">';
    for (let diagnostic of diagnostics){
        let stack = diagnostic.frames.length ? diagnostic.frames.reduce((p, frame)=>{
            return `${p}
<a href="/__parcel_launch_editor?file=${encodeURIComponent(frame.location)}" style="text-decoration: underline; color: #888" onclick="fetch(this.href); return false">${frame.location}</a>
${frame.code}`;
        }, "") : diagnostic.stack;
        errorHTML += `
      <div>
        <div style="font-size: 18px; font-weight: bold; margin-top: 20px;">
          🚨 ${diagnostic.message}
        </div>
        <pre>${stack}</pre>
        <div>
          ${diagnostic.hints.map((hint)=>"<div>\uD83D\uDCA1 " + hint + "</div>").join("")}
        </div>
        ${diagnostic.documentation ? `<div>📝 <a style="color: violet" href="${diagnostic.documentation}" target="_blank">Learn more</a></div>` : ""}
      </div>
    `;
    }
    errorHTML += "</div>";
    overlay.innerHTML = errorHTML;
    return overlay;
}
function fullReload() {
    if ("reload" in location) location.reload();
    else if (extCtx && extCtx.runtime && extCtx.runtime.reload) extCtx.runtime.reload();
}
function getParents(bundle, id) /*: Array<[ParcelRequire, string]> */ {
    var modules = bundle.modules;
    if (!modules) return [];
    var parents = [];
    var k, d, dep;
    for(k in modules)for(d in modules[k][1]){
        dep = modules[k][1][d];
        if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) parents.push([
            bundle,
            k
        ]);
    }
    if (bundle.parent) parents = parents.concat(getParents(bundle.parent, id));
    return parents;
}
function updateLink(link) {
    var newLink = link.cloneNode();
    newLink.onload = function() {
        if (link.parentNode !== null) // $FlowFixMe
        link.parentNode.removeChild(link);
    };
    newLink.setAttribute("href", link.getAttribute("href").split("?")[0] + "?" + Date.now()); // $FlowFixMe
    link.parentNode.insertBefore(newLink, link.nextSibling);
}
var cssTimeout = null;
function reloadCSS() {
    if (cssTimeout) return;
    cssTimeout = setTimeout(function() {
        var links = document.querySelectorAll('link[rel="stylesheet"]');
        for(var i = 0; i < links.length; i++){
            // $FlowFixMe[incompatible-type]
            var href = links[i].getAttribute("href");
            var hostname = getHostname();
            var servedFromHMRServer = hostname === "localhost" ? new RegExp("^(https?:\\/\\/(0.0.0.0|127.0.0.1)|localhost):" + getPort()).test(href) : href.indexOf(hostname + ":" + getPort());
            var absolute = /^https?:\/\//i.test(href) && href.indexOf(location.origin) !== 0 && !servedFromHMRServer;
            if (!absolute) updateLink(links[i]);
        }
        cssTimeout = null;
    }, 50);
}
function hmrDownload(asset) {
    if (asset.type === "js") {
        if (typeof document !== "undefined") {
            let script = document.createElement("script");
            script.src = asset.url + "?t=" + Date.now();
            if (asset.outputFormat === "esmodule") script.type = "module";
            return new Promise((resolve, reject)=>{
                var _document$head;
                script.onload = ()=>resolve(script);
                script.onerror = reject;
                (_document$head = document.head) === null || _document$head === void 0 || _document$head.appendChild(script);
            });
        } else if (typeof importScripts === "function") {
            // Worker scripts
            if (asset.outputFormat === "esmodule") return import(asset.url + "?t=" + Date.now());
            else return new Promise((resolve, reject)=>{
                try {
                    importScripts(asset.url + "?t=" + Date.now());
                    resolve();
                } catch (err) {
                    reject(err);
                }
            });
        }
    }
}
async function hmrApplyUpdates(assets) {
    global.parcelHotUpdate = Object.create(null);
    let scriptsToRemove;
    try {
        // If sourceURL comments aren't supported in eval, we need to load
        // the update from the dev server over HTTP so that stack traces
        // are correct in errors/logs. This is much slower than eval, so
        // we only do it if needed (currently just Safari).
        // https://bugs.webkit.org/show_bug.cgi?id=137297
        // This path is also taken if a CSP disallows eval.
        if (!supportsSourceURL) {
            let promises = assets.map((asset)=>{
                var _hmrDownload;
                return (_hmrDownload = hmrDownload(asset)) === null || _hmrDownload === void 0 ? void 0 : _hmrDownload.catch((err)=>{
                    // Web extension bugfix for Chromium
                    // https://bugs.chromium.org/p/chromium/issues/detail?id=1255412#c12
                    if (extCtx && extCtx.runtime && extCtx.runtime.getManifest().manifest_version == 3) {
                        if (typeof ServiceWorkerGlobalScope != "undefined" && global instanceof ServiceWorkerGlobalScope) {
                            extCtx.runtime.reload();
                            return;
                        }
                        asset.url = extCtx.runtime.getURL("/__parcel_hmr_proxy__?url=" + encodeURIComponent(asset.url + "?t=" + Date.now()));
                        return hmrDownload(asset);
                    }
                    throw err;
                });
            });
            scriptsToRemove = await Promise.all(promises);
        }
        assets.forEach(function(asset) {
            hmrApply(module.bundle.root, asset);
        });
    } finally{
        delete global.parcelHotUpdate;
        if (scriptsToRemove) scriptsToRemove.forEach((script)=>{
            if (script) {
                var _document$head2;
                (_document$head2 = document.head) === null || _document$head2 === void 0 || _document$head2.removeChild(script);
            }
        });
    }
}
function hmrApply(bundle, asset) {
    var modules = bundle.modules;
    if (!modules) return;
    if (asset.type === "css") reloadCSS();
    else if (asset.type === "js") {
        let deps = asset.depsByBundle[bundle.HMR_BUNDLE_ID];
        if (deps) {
            if (modules[asset.id]) {
                // Remove dependencies that are removed and will become orphaned.
                // This is necessary so that if the asset is added back again, the cache is gone, and we prevent a full page reload.
                let oldDeps = modules[asset.id][1];
                for(let dep in oldDeps)if (!deps[dep] || deps[dep] !== oldDeps[dep]) {
                    let id = oldDeps[dep];
                    let parents = getParents(module.bundle.root, id);
                    if (parents.length === 1) hmrDelete(module.bundle.root, id);
                }
            }
            if (supportsSourceURL) // Global eval. We would use `new Function` here but browser
            // support for source maps is better with eval.
            (0, eval)(asset.output);
             // $FlowFixMe
            let fn = global.parcelHotUpdate[asset.id];
            modules[asset.id] = [
                fn,
                deps
            ];
        } else if (bundle.parent) hmrApply(bundle.parent, asset);
    }
}
function hmrDelete(bundle, id) {
    let modules = bundle.modules;
    if (!modules) return;
    if (modules[id]) {
        // Collect dependencies that will become orphaned when this module is deleted.
        let deps = modules[id][1];
        let orphans = [];
        for(let dep in deps){
            let parents = getParents(module.bundle.root, deps[dep]);
            if (parents.length === 1) orphans.push(deps[dep]);
        } // Delete the module. This must be done before deleting dependencies in case of circular dependencies.
        delete modules[id];
        delete bundle.cache[id]; // Now delete the orphans.
        orphans.forEach((id)=>{
            hmrDelete(module.bundle.root, id);
        });
    } else if (bundle.parent) hmrDelete(bundle.parent, id);
}
function hmrAcceptCheck(bundle, id, depsByBundle) {
    if (hmrAcceptCheckOne(bundle, id, depsByBundle)) return true;
     // Traverse parents breadth first. All possible ancestries must accept the HMR update, or we'll reload.
    let parents = getParents(module.bundle.root, id);
    let accepted = false;
    while(parents.length > 0){
        let v = parents.shift();
        let a = hmrAcceptCheckOne(v[0], v[1], null);
        if (a) // If this parent accepts, stop traversing upward, but still consider siblings.
        accepted = true;
        else {
            // Otherwise, queue the parents in the next level upward.
            let p = getParents(module.bundle.root, v[1]);
            if (p.length === 0) {
                // If there are no parents, then we've reached an entry without accepting. Reload.
                accepted = false;
                break;
            }
            parents.push(...p);
        }
    }
    return accepted;
}
function hmrAcceptCheckOne(bundle, id, depsByBundle) {
    var modules = bundle.modules;
    if (!modules) return;
    if (depsByBundle && !depsByBundle[bundle.HMR_BUNDLE_ID]) {
        // If we reached the root bundle without finding where the asset should go,
        // there's nothing to do. Mark as "accepted" so we don't reload the page.
        if (!bundle.parent) return true;
        return hmrAcceptCheck(bundle.parent, id, depsByBundle);
    }
    if (checkedAssets[id]) return true;
    checkedAssets[id] = true;
    var cached = bundle.cache[id];
    assetsToAccept.push([
        bundle,
        id
    ]);
    if (!cached || cached.hot && cached.hot._acceptCallbacks.length) return true;
}
function hmrAcceptRun(bundle, id) {
    var cached = bundle.cache[id];
    bundle.hotData = {};
    if (cached && cached.hot) cached.hot.data = bundle.hotData;
    if (cached && cached.hot && cached.hot._disposeCallbacks.length) cached.hot._disposeCallbacks.forEach(function(cb) {
        cb(bundle.hotData);
    });
    delete bundle.cache[id];
    bundle(id);
    cached = bundle.cache[id];
    if (cached && cached.hot && cached.hot._acceptCallbacks.length) cached.hot._acceptCallbacks.forEach(function(cb) {
        var assetsToAlsoAccept = cb(function() {
            return getParents(module.bundle.root, id);
        });
        if (assetsToAlsoAccept && assetsToAccept.length) // $FlowFixMe[method-unbinding]
        assetsToAccept.push.apply(assetsToAccept, assetsToAlsoAccept);
    });
    acceptedAssets[id] = true;
}

},{}],"h7u1C":[function(require,module,exports) {
var _png = require("../public/image/*.png");
console.log("jeu Paper Forest");
let aboutTrancher = "Une attaque rapide de 3";
let aboutGriffe = "Une petite attaque de 2";
let aboutProteger = "Se d\xe9fendre contre la prochaine attaque de 2";
let aboutAttaqueChargee = "Une grosse attaque de 6 qui demande un tour de pr\xe9paration";
let aboutEsquive = "Tenter d'esquiver la prochaine attaque";
let noCharacter = {
    name: "",
    imgLeft: "",
    imgRight: "",
    abilities: [],
    pv: 10,
    pvMax: 10,
    mainFighter: false,
    armor: 0
};
let ecureuil = {
    name: "Ecureuil",
    imgLeft: _png.ecureuil_left,
    imgRight: _png.ecureuil_right,
    abilities: [
        {
            name: "Esquive",
            value: 0.6,
            about: aboutEsquive,
            on: false
        },
        {
            name: "Griffe",
            value: 2,
            about: aboutGriffe
        }
    ],
    pv: 8,
    pvMax: 8,
    mainFighter: false,
    armor: 0
};
let hibou = {
    name: "Hibou",
    imgLeft: _png.hibou_left,
    imgRight: _png.hibou_right,
    abilities: [
        {
            name: "Griffe",
            value: 2,
            about: aboutGriffe
        },
        {
            name: "Attaque charg\xe9e",
            value: 6,
            about: aboutAttaqueChargee,
            on: false
        },
        {
            name: "Esquive",
            value: 0.5,
            about: aboutEsquive,
            on: false
        }
    ],
    pv: 10,
    pvMax: 10,
    mainFighter: false,
    armor: 0
};
let mamanRenard = {
    name: "Renarde",
    imgLeft: _png.maman_renard_left,
    imgRight: _png.maman_renard_right,
    abilities: [
        {
            name: "Trancher",
            value: 3,
            about: aboutTrancher
        },
        {
            name: "Prot\xe9ger",
            value: 2,
            about: aboutProteger,
            on: false
        },
        {
            name: "Esquive",
            value: 0.4,
            about: aboutEsquive,
            on: false
        }
    ],
    pv: 12,
    pvMax: 12,
    mainFighter: false,
    armor: 0
};
let ours = {
    name: "Ours",
    imgLeft: _png.ours_left,
    imgRight: _png.ours_right,
    abilities: [
        {
            name: "Trancher",
            value: 3,
            about: aboutTrancher
        },
        {
            name: "Attaque charg\xe9e",
            value: 6,
            about: aboutAttaqueChargee,
            on: false
        },
        {
            name: "Prot\xe9ger",
            value: 2,
            about: aboutProteger,
            on: false
        }
    ],
    pv: 10,
    pvMax: 10,
    mainFighter: false,
    armor: 0
};
let petitRenard = {
    name: "petit Renard",
    imgLeft: _png.petit_renard_2_left,
    imgRight: _png.petit_renard_2_right,
    abilities: [],
    pv: 10,
    pvMax: 10,
    mainFighter: false,
    armor: 0
};
let tsuru = {
    name: "Tsuru",
    imgLeft: _png.rose_cropped_left,
    imgRight: _png.rose_cropped_right,
    abilities: [],
    pv: 10,
    pvMax: 10,
    mainFighter: false,
    armor: 0
};
let tabAllChapters = [
    [
        {
            characterLeft: tsuru,
            characterRight: ecureuil,
            talkingCharacter: noCharacter,
            dialogues: [
                "Dans la for\xeat du Pli, que l'on appelle aussi Oritatami no mori, vivent des Origamins, des cr\xe9atures de papiers ayant pris vie. En temps normal, des mikos, pr\xeatresses se consacrant \xe0 l'art de l'origami veillent sur eux. La princesse Tsuru r\xe8gne sur la for\xeat du Pli et tout ses habitants.",
                "Mais un jour la princesse du Th\xe9 hivernal jeta un sortil\xe8ge sur la for\xeat du pli. Les Origamins, influenc\xe9s par son emprise mal\xe9fique devinrent aggressifs. Les mikos fuirent, et de la princesse Tsuru on n'eut plus de nouvelles.",
                "Une Renarde origamine \xe9tait en voyage lorsque le sortil\xe8ge fut lanc\xe9. Lorsqu'elle revint, elle se mit en t\xeate de combattre tout les Origamins mal\xe9fique jusqu'\xe0 retrouver son petit"
            ]
        },
        {
            characterLeft: mamanRenard,
            characterRight: noCharacter,
            talkingCharacter: mamanRenard,
            dialogues: [
                "Ils sont tous devenus fous ! Je ne peux rien y faire ! Il ne reste plus qu'\xe0 me battre jusqu'\xe0 le retrouver !",
                "Craignez-moi !"
            ]
        },
        {
            characterLeft: mamanRenard,
            characterRight: hibou,
            talkingCharacter: mamanRenard,
            dialogues: [
                "Hibou ! Mon ami ! Tu ne me reconnais donc pas ?"
            ]
        },
        {
            characterLeft: mamanRenard,
            characterRight: hibou,
            talkingCharacter: hibou,
            dialogues: [
                "AAARRRRAAAGGHH"
            ]
        }
    ],
    [
        {
            characterLeft: mamanRenard,
            characterRight: hibou,
            talkingCharacter: hibou,
            dialogues: [
                "Je suis d\xe9sol\xe9e de t'avoir attaqu\xe9e Renarde, je ne sais pas ce qui m'a pris",
                "J'esp\xe8re que tu me pardonneras !"
            ]
        },
        {
            characterLeft: mamanRenard,
            characterRight: hibou,
            talkingCharacter: mamanRenard,
            dialogues: [
                "Ce n'est pas ta faute Hibou, tu \xe9tais sous l'influence du sortil\xe8ge de la Princesse du th\xe9 hivernal.",
                "Je cherche mon petit, je vais continuer mon chemin dans la for\xeat jusqu'\xe0 le retrouver"
            ]
        },
        {
            characterLeft: mamanRenard,
            characterRight: hibou,
            talkingCharacter: hibou,
            dialogues: [
                "Attends, je vais t'accompagner ! A deux nous seront plus forts !",
                "Attention, voil\xe0 Ours ! Il va nous attaquer !"
            ]
        },
        {
            characterLeft: mamanRenard,
            characterRight: ours,
            talkingCharacter: ours,
            dialogues: [
                "Je vais vous d\xe9chiqueter ! !"
            ]
        }
    ],
    [
        {
            characterLeft: mamanRenard,
            characterRight: ours,
            talkingCharacter: ours,
            dialogues: [
                "Oh, pardon Renarde ! Pardon Hibou ! Vous allez bien ?",
                "Je ne vous ai pas fait trop de d\xe9g\xe2ts ?"
            ]
        },
        {
            characterLeft: mamanRenard,
            characterRight: ours,
            talkingCharacter: mamanRenard,
            dialogues: [
                "Oui ne t'inqui\xe8te pas !",
                "Tu veux nous accompagner ? Je cherche mon renardeau !"
            ]
        },
        {
            characterLeft: hibou,
            characterRight: ecureuil,
            talkingCharacter: hibou,
            dialogues: [
                "Attention, Ecureuil se jette sur nous !"
            ]
        }
    ],
    [
        {
            characterLeft: hibou,
            characterRight: ecureuil,
            talkingCharacter: ecureuil,
            dialogues: [
                "Oh ! Je suis \xe0 nouveau moi-m\xeame ! Merci beaucoup ! ",
                "J'ai vu Renardeau cach\xe9 derri\xe8re ce bosquet, venez, je vais vous mener \xe0 lui",
                "Suivez-moi !"
            ]
        },
        {
            characterLeft: mamanRenard,
            characterRight: petitRenard,
            talkingCharacter: petitRenard,
            dialogues: [
                "Maman ! Maman !!!",
                "Mon b\xe9b\xe9 !"
            ]
        },
        {
            characterLeft: mamanRenard,
            characterRight: petitRenard,
            talkingCharacter: noCharacter,
            dialogues: [
                "Et c'est ainsi que l'aventure de Renarde prit fin. Mais peut-\xeatre affrontera-t-elle d'autres ennemis dans une prochaine aventure !"
            ]
        }
    ]
];
let tabChoices = [
    [
        mamanRenard
    ],
    [
        mamanRenard,
        hibou
    ],
    [
        mamanRenard,
        hibou,
        ours
    ]
];
let tabOpponents = [
    hibou,
    ours,
    ecureuil
];
let indexDialogue = 0;
let indexTab = 0;
let indexOfChoice = 0;
let interfaceBox = document.querySelector(".interface-box");
let characterLeft = document.querySelector(".character-left");
let characterRight = document.querySelector(".character-right");
const rightContainer = document.querySelector(".right-container");
const leftContainer = document.querySelector(".left-container");
let mainWindow = document.querySelector(".main-window");
let innerMenu = document.querySelector(".inner-menu");
const menu = document.querySelector(".menu");
let dodged = false;
let lastFighterAction;
let lastOpponentAction;
dialogueAndchoices(tabAllChapters[indexOfChoice]);
/**
 * Tour de bataille du combattant contrôlé par la joueuse
 * @param fighter Le combattant
 * @param actions Le tableau d'abilities du combattant
 * @param opponent L'adversaire
 */ function battleBegin(fighter, actions, opponent) {
    indexOfChoice++;
    fighter.mainFighter = true;
    if (characterLeft) characterLeft.src = fighter.imgLeft;
    if (characterRight) characterRight.src = opponent.imgRight;
    if (leftContainer) leftContainer.innerHTML = `<h4>${fighter.name}</h4><p>Points de vie : <span class="pv-number-left">${fighter.pv}/${fighter.pvMax}</span></p>
        <div class="progress margin-bottom">
          <div class="bar warning w-100 bar-left"></div>
        </div>
        <div class="ability-property">
        </div>`;
    if (rightContainer) rightContainer.innerHTML = `<h4>${opponent.name}</h4><p>Points de vie : <span class="pv-number-right">${opponent.pv}/${opponent.pvMax}</span></p>
        <div class="progress margin-bottom">
            <div class="bar warning w-100 bar-right">
            </div>
        </div>
        <div class="ability-property-opponent">
        </div>`;
    if (interfaceBox) {
        interfaceBox.classList.add("row");
        interfaceBox.innerHTML = `<div class="div-abilities colonne col-4"></div>
        <div class="middle col-8 text-center">Bataille !</div>`;
    }
    showPV(fighter, "left");
    showPV(opponent, "right");
    return fighterBattle(fighter, actions, opponent);
}
/**
 * Lance le combat
 * @param fighter combattant principal
 * @param actions abilities du combattant principal
 * @param opponent opposant du combat
 */ function fighterBattle(fighter, actions, opponent) {
    if (leftContainer && rightContainer) {
        leftContainer.style.opacity = "1";
        rightContainer.style.opacity = "1";
    }
    let divAbilities = document.querySelector(".div-abilities");
    let abilityProperty = document.querySelector(".ability-property");
    for(let i = 0; i < actions.length; i++){
        let divAbility = document.createElement("div");
        if (divAbilities) divAbilities.appendChild(divAbility);
        let nameAbility = document.createElement("h5");
        if (divAbility) divAbility.appendChild(nameAbility);
        nameAbility.innerHTML = actions[i].name;
        divAbility?.addEventListener("mouseover", ()=>{
            if (abilityProperty) abilityProperty.innerHTML = `<p>${actions[i].name}</p><p>${actions[i].about}</p>`;
        });
        divAbility?.addEventListener("mouseout", ()=>{
            if (abilityProperty) abilityProperty.innerHTML = "";
        });
        divAbility?.addEventListener("click", ()=>{
            if (divAbilities) divAbilities.innerHTML = "";
            battleAction(fighter, actions[i], opponent, "right");
        });
    }
}
/**
 * Ce qu'il se passe une fois que l'action a été choisie pendant la bataille
 * @param action L'action choisie par le combattant
 * @param opponent L'adversaire
 * @returns 
 */ function battleAction(fighter, action, opponent, opposantSide) {
    let middleContent = document.querySelector(".middle");
    let abilityProperty = document.querySelector(".ability-property");
    if (abilityProperty) abilityProperty.innerHTML = "";
    let abilityPropertyOpponent = document.querySelector(".ability-property-opponent");
    if (abilityPropertyOpponent) abilityPropertyOpponent.innerHTML = "";
    let damage = action.value - opponent.armor;
    if (damage > opponent.pv) damage = opponent.pv;
    if (action.name == "Attaque charg\xe9e") {
        if (action.on == false) {
            action.on = true;
            dodged = false;
            if (middleContent) middleContent.innerHTML = `${fighter.name} charge l'attaque`;
        } else {
            action.on = false;
            if (dodged == true) {
                if (middleContent) middleContent.innerHTML = `${opponent.name} esquive l'attaque`;
                dodged = false;
            } else {
                opponent.pv -= action.value - opponent.armor;
                if (opponent.pv < 0) opponent.pv = 0;
                if (middleContent) middleContent.innerHTML = `${opponent.name} perd ${damage} pv`;
            }
        }
    }
    if (dodged == true) {
        if (middleContent) middleContent.innerHTML = `${opponent.name} esquive l'attaque`;
        dodged = false;
    } else {
        if (action.name == "Trancher") {
            opponent.pv -= damage;
            if (middleContent) middleContent.innerHTML = `${opponent.name} perd ${damage} pv`;
        }
        if (action.name == "Griffe") {
            opponent.pv -= damage;
            if (middleContent) middleContent.innerHTML = `${opponent.name} perd ${damage} pv`;
        }
    }
    if (action.name == "Prot\xe9ger") {
        fighter.armor = action.value;
        if (middleContent) middleContent.innerHTML = `Protégé.e de ${action.value} pour la prochaine attaque !`;
        dodged = false;
    }
    if (action.name === "Esquive") {
        if (Math.random() < action.value) {
            dodged = true;
            if (middleContent) middleContent.innerHTML = "Esquive de la prochaine attaque r\xe9ussie !";
        } else {
            dodged = false;
            if (middleContent) middleContent.innerHTML = "Esquive de la prochaine attaque rat\xe9e !";
        }
    }
    const pvNumber = document.querySelector(`.pv-number-${opposantSide}`);
    if (pvNumber) pvNumber.innerHTML = `${opponent.pv}/${opponent.pvMax}`;
    showPV(opponent, opposantSide);
    opponent.armor = 0;
    if (fighter.mainFighter == true) lastFighterAction = action;
    else lastOpponentAction = action;
    setTimeout(()=>{
        if (opponent.pv <= 0) {
            if (fighter.mainFighter == true) {
                if (middleContent) middleContent.innerHTML = "Victoire ! Fin du combat !";
                setTimeout(()=>{
                    fighter.pv = fighter.pvMax;
                    opponent.pv = opponent.pvMax;
                    dialogueAndchoices(tabAllChapters[indexOfChoice]);
                }, 1000);
                return;
            } else {
                if (middleContent) middleContent.innerHTML = "D\xe9faite ! Vous avez perdu !";
                if (interfaceBox) {
                    interfaceBox.innerHTML = `<button class="start-again">Recommencer le combat ?</button>`;
                    interfaceBox.classList.add("centered");
                }
                let startAgainBtn = document.querySelector(".start-again");
                startAgainBtn?.addEventListener("click", ()=>{
                    opponent.pv = opponent.pvMax;
                    fighter.pv = fighter.pvMax;
                    if (interfaceBox) interfaceBox.classList.remove("centered");
                    battleBegin(opponent, opponent.abilities, fighter);
                });
                return;
            }
        }
        if (opponent.mainFighter == false) {
            opponentBattle(opponent, fighter);
            return;
        } else {
            fighterBattle(opponent, opponent.abilities, fighter);
            return;
        }
    }, 2000);
}
/**
 * Update l'affichage des pv d'un personnage
 * @param character le personnage en question
 * @param characterSide le côté de l'écran du personnage
 */ function showPV(character, characterSide) {
    const pvNumber = document.querySelector(`.pv-number-${characterSide}`);
    if (pvNumber) pvNumber.innerHTML = `${character.pv}/${character.pvMax}`;
    const bar = document.querySelector(`.bar-${characterSide}`);
    if (bar) {
        for(let i = bar.classList.length - 1; i >= 0; i--){
            const className = bar.classList[i];
            if (className.startsWith("w-")) bar.classList.remove(className);
        }
        let barPercent = Math.round(character.pv / character.pvMax * 100);
        bar.classList.add(`w-${barPercent}`);
    }
}
/**
 * Le tour du combattant
 * @param opponent L'opposant du combat
 * @param figther Le combattant principal
 */ function opponentBattle(opponent, fighter) {
    let middleContent = document.querySelector(".middle");
    setTimeout(()=>{
        if (middleContent) middleContent.innerHTML = `C'est au tour de ${opponent.name} !`;
    }, 500);
    let choosenAction = opponent.abilities[Math.floor(Math.random() * opponent.abilities.length)];
    setTimeout(()=>{
        if (middleContent) {
            middleContent.innerHTML = `${opponent.name} utilise ${choosenAction.name}!`;
            let abilityPropertyOpponent = document.querySelector(".ability-property-opponent");
            if (abilityPropertyOpponent) abilityPropertyOpponent.innerHTML = `<p>${choosenAction.name}</p><p>${choosenAction.about}</p>`;
        }
    }, 2000);
    setTimeout(()=>{
        battleAction(opponent, choosenAction, fighter, "left");
    }, 4000);
    setTimeout(()=>{
        if (middleContent) middleContent.innerHTML = `C'est au tour de ${fighter.name} !`;
    }, 6000);
}
/**
 * Permet de lancer et afficher les dialogues entre personnages
 * @param currentTabChapter Tableaux de dialogues
 */ function dialogueAndchoices(currentTabChapter) {
    if (interfaceBox) interfaceBox.classList.remove("row");
    if (leftContainer && rightContainer) {
        leftContainer.style.opacity = "0";
        rightContainer.style.opacity = "0";
    }
    indexDialogue = 0;
    indexTab = 0;
    showDialogue(currentTabChapter);
    interfaceBox?.addEventListener("click", function loopDialogues() {
        if (indexTab < currentTabChapter.length) {
            if (indexDialogue + 1 < currentTabChapter[indexTab].dialogues.length) {
                indexDialogue++;
                showDialogue(currentTabChapter);
                if (indexDialogue + 1 == currentTabChapter[indexTab].dialogues.length) {
                    indexTab++;
                    indexDialogue = -1;
                }
            }
        } else {
            if (interfaceBox) interfaceBox.removeEventListener("click", loopDialogues);
            if (indexOfChoice == 0) battleBegin(mamanRenard, mamanRenard.abilities, hibou);
            else if (indexOfChoice < 3) showChoice();
            else if (interfaceBox) {
                if (interfaceBox) interfaceBox.classList.add("centered");
                interfaceBox.innerHTML = "Bravo ! Fin du jeu !";
            }
            return;
        }
    });
}
/**
 * Montre les dialogues et les personnages de ces dialogues
 * @param currentTabChapter Tableau des dialogue
 * @returns 
 */ function showDialogue(currentTabChapter) {
    if (interfaceBox) interfaceBox.innerHTML = `<h4 class="character-heading"></h4>
        <p class="dialogue-para"></p>`;
    let dialoguePara = document.querySelector(".dialogue-para");
    let characterHeading = document.querySelector(".character-heading");
    if (dialoguePara) dialoguePara.innerHTML = `${currentTabChapter[indexTab].dialogues[indexDialogue]} <span class="dots border-bottom">.   .   .</span>`;
    if (characterHeading) characterHeading.innerHTML = `${currentTabChapter[indexTab].talkingCharacter.name}`;
    if (characterLeft) characterLeft.src = currentTabChapter[indexTab].characterLeft.imgLeft;
    if (characterRight) characterRight.src = currentTabChapter[indexTab].characterRight.imgRight;
    return;
}
/**
 * Offre un choix dans la narration
 * @param tabOfChoices Tableau des différents choix
 * @returns 
 */ function showChoice() {
    if (mainWindow) mainWindow.style.display = "none";
    if (interfaceBox) interfaceBox.innerHTML = "";
    if (menu && innerMenu) {
        menu.style.display = "flex";
        const choiceQuestion = document.createElement("h5");
        choiceQuestion.classList.add("margin-none");
        choiceQuestion.innerHTML = `<h3 class='text-center'>Choisissez votre combattant : </h3>`;
        innerMenu.appendChild(choiceQuestion);
        let divFighters = document.createElement("div");
        divFighters.classList.add("row", "flex-spaces", "tabs");
        innerMenu.appendChild(divFighters);
        for (const [index, fighter] of tabChoices[indexOfChoice].entries()){
            let index1 = index + 1;
            if (index == 0) divFighters.innerHTML += `
                <input id="tab${index1}" type="radio" name="tabs" checked>
                <label for="tab${index1}">${fighter.name}</label>`;
            else divFighters.innerHTML += `
                <input id="tab${index1}" type="radio" name="tabs">
                <label for="tab${index1}">${fighter.name}</label>`;
        }
        for (const [index2, fighter1] of tabChoices[indexOfChoice].entries()){
            let index11 = index2 + 1;
            divFighters.innerHTML += `
            <div class="content margin" id="content${index11}">
            <h4 class="margin-left-none">${fighter1.name}</h4>
            <p><span class="pv-title">PV : </span>${fighter1.pv}/${fighter1.pvMax}</p>
            <h5>Capacités : </h5>
            </div>`;
            let divContent = document.querySelector(`#content${index11}`);
            for (const ability of fighter1.abilities)if (divContent) divContent.innerHTML += `<p>${ability.name} : ${ability.about}</p>`;
            if (divContent) divContent.innerHTML += `<button class="margin-top-large" id="btn${index2}">Choisir ${fighter1.name}</button>`;
        }
    }
    let btn0 = document.querySelector(`#btn0`);
    let btn1 = document.querySelector(`#btn1`);
    let btn2 = document.querySelector(`#btn2`);
    /**
     * Permet de gérer l'addEventListener des boutons pour choisir le combattant
     * @param button Le bouton sélectionné
     * @param index L'index dans le tableau de choix de combattant de notre combattant
     */ function choosingFighterBtn(button, index) {
        if (button) button.addEventListener("click", ()=>{
            if (menu && innerMenu) {
                innerMenu.innerHTML = "";
                menu.style.display = "none";
            }
            if (mainWindow) mainWindow.style.display = "block";
            battleBegin(tabChoices[indexOfChoice][index], tabChoices[indexOfChoice][index].abilities, tabOpponents[indexOfChoice]);
        });
    }
    if (btn0) choosingFighterBtn(btn0, 0);
    if (btn1) choosingFighterBtn(btn1, 1);
    if (btn2) choosingFighterBtn(btn2, 2);
}

},{"../public/image/*.png":"3L1N4"}],"3L1N4":[function(require,module,exports) {
const _temp0 = require("52d3fb2970e2895c");
const _temp1 = require("1adc7dee0391cb51");
const _temp2 = require("2c30f599c022bb56");
const _temp3 = require("ff7a3b23599c989e");
const _temp4 = require("89519498af788b6");
const _temp5 = require("443ff230daaeb82a");
const _temp6 = require("619e3e39adfd274e");
const _temp7 = require("d63c0697a80c34d3");
const _temp8 = require("3542d22cb86d21d0");
const _temp9 = require("c21baee6663573fd");
const _temp10 = require("1f8356239b2e30a3");
const _temp11 = require("cb453dae5b626249");
const _temp12 = require("a0bed35c621a8196");
const _temp13 = require("4e27507eaf46589a");
const _temp14 = require("e9548aaa0eb3c82");
const _temp15 = require("284f52737d9cbc0a");
const _temp16 = require("a58f343ea539310d");
const _temp17 = require("60eeddea1a581eea");
const _temp18 = require("20c41a19b319a0d7");
const _temp19 = require("3cc237832ba30f");
const _temp20 = require("7ee4bc6856a0e702");
const _temp21 = require("47dacda3b91b6a86");
const _temp22 = require("13f277d0b9ba24ca");
const _temp23 = require("49f049869c8f94d3");
const _temp24 = require("c7db2690078ba10a");
const _temp25 = require("d6e8287642afe48d");
const _temp26 = require("f25f73cf1afcee88");
const _temp27 = require("678d9f23ff8c0b2c");
const _temp28 = require("140765b83ba1e3fc");
const _temp29 = require("a359551a57b01545");
const _temp30 = require("7c918d1407615ccc");
const _temp31 = require("cc30520e2eac9628");
const _temp32 = require("daa7ac84f057b894");
const _temp33 = require("eeb9549cdb8561da");
const _temp34 = require("cb69f52cf006e7d1");
const _temp35 = require("4052f4acf7101026");
const _temp36 = require("44ad42b536cdd721");
const _temp37 = require("184a1039b8f56992");
const _temp38 = require("45ad71e2f2af7334");
const _temp39 = require("4a15b6003be4bc4a");
const _temp40 = require("8e40a38a9f89bd62");
const _temp41 = require("18d3451abe84ce85");
const _temp42 = require("ce6ee33c0190a17e");
const _temp43 = require("e2b147d94fca273d");
const _temp44 = require("62476c32e5fad03a");
const _temp45 = require("8360361f5449b57c");
const _temp46 = require("289b2fc71c24eade");
const _temp47 = require("3a5b591aa7bddb96");
const _temp48 = require("b2f3ebabd8abfd30");
const _temp49 = require("55868f32fe562a01");
const _temp50 = require("efe172ee5d4ef465");
const _temp51 = require("bce1b451addde72c");
const _temp52 = require("c2a85d451aa35e18");
const _temp53 = require("402b55e92f3dbb19");
const _temp54 = require("d50e77b23f1eafe0");
const _temp55 = require("fb6beaca94846572");
module.exports = {
    "bleu": _temp0,
    "bleu_left": _temp1,
    "bleu_right": _temp2,
    "dauphin": _temp3,
    "dauphin_left": _temp4,
    "dauphin_right": _temp5,
    "dragon": _temp6,
    "dragon_left": _temp7,
    "dragon_right": _temp8,
    "ecureuil": _temp9,
    "ecureuil_left": _temp10,
    "ecureuil_right": _temp11,
    "foret_papier": _temp12,
    "foret_papier_2": _temp13,
    "foret_papier_3": _temp14,
    "foret_papier_4": _temp15,
    "grenouille": _temp16,
    "grenouille_left": _temp17,
    "grenouille_right": _temp18,
    "grue_2": _temp19,
    "grue_2_left": _temp20,
    "grue_2_right": _temp21,
    "hibou": _temp22,
    "hibou_left": _temp23,
    "hibou_right": _temp24,
    "hirondelle": _temp25,
    "hirondelle_left": _temp26,
    "hirondelle_right": _temp27,
    "maman_renard": _temp28,
    "maman_renard_left": _temp29,
    "maman_renard_right": _temp30,
    "ours": _temp31,
    "ours_left": _temp32,
    "ours_right": _temp33,
    "papillon": _temp34,
    "papillon_left": _temp35,
    "papillon_right": _temp36,
    "petit_renard": _temp37,
    "petit_renard_2": _temp38,
    "petit_renard_2_left": _temp39,
    "petit_renard_2_right": _temp40,
    "poisson": _temp41,
    "poisson_left": _temp42,
    "poisson_right": _temp43,
    "poupees": _temp44,
    "renard": _temp45,
    "requin": _temp46,
    "requin_left": _temp47,
    "requin_right": _temp48,
    "rose_cropped_left": _temp49,
    "rose_cropped_right": _temp50,
    "rose_left": _temp51,
    "rose_right": _temp52,
    "sanglier": _temp53,
    "sanglier_left": _temp54,
    "sanglier_right": _temp55
};

},{"52d3fb2970e2895c":"lWtUR","1adc7dee0391cb51":"3gpNe","2c30f599c022bb56":"6YS4U","ff7a3b23599c989e":"6ob0U","89519498af788b6":"dMlsJ","443ff230daaeb82a":"2CZKI","619e3e39adfd274e":"d31qj","d63c0697a80c34d3":"iWtfW","3542d22cb86d21d0":"1VweF","c21baee6663573fd":"cCexY","1f8356239b2e30a3":"6oSdW","cb453dae5b626249":"eM2ng","a0bed35c621a8196":"kriua","4e27507eaf46589a":"9OFE7","e9548aaa0eb3c82":"flgNE","284f52737d9cbc0a":"iHOnz","a58f343ea539310d":"BVXzg","60eeddea1a581eea":"hvVXZ","20c41a19b319a0d7":"6vAUN","3cc237832ba30f":"64e8A","7ee4bc6856a0e702":"c0TI7","47dacda3b91b6a86":"8JA6r","13f277d0b9ba24ca":"7OjXt","49f049869c8f94d3":"2Ct0k","c7db2690078ba10a":"hf8mY","d6e8287642afe48d":"7pI4l","f25f73cf1afcee88":"kiJev","678d9f23ff8c0b2c":"3YY5r","140765b83ba1e3fc":"1YsnD","a359551a57b01545":"6vzLg","7c918d1407615ccc":"i0jL7","cc30520e2eac9628":"6N4el","daa7ac84f057b894":"7OEng","eeb9549cdb8561da":"aNHz5","cb69f52cf006e7d1":"3R36i","4052f4acf7101026":"fCKSi","44ad42b536cdd721":"aBBz9","184a1039b8f56992":"6q0Fz","45ad71e2f2af7334":"6S1Sx","4a15b6003be4bc4a":"aV5t8","8e40a38a9f89bd62":"123Ca","18d3451abe84ce85":"aPrl7","ce6ee33c0190a17e":"hcDPQ","e2b147d94fca273d":"9u79o","62476c32e5fad03a":"jW09L","8360361f5449b57c":"bBXfb","289b2fc71c24eade":"1CoXX","3a5b591aa7bddb96":"iMPCt","b2f3ebabd8abfd30":"dvRno","55868f32fe562a01":"4xBT2","efe172ee5d4ef465":"e782M","bce1b451addde72c":"7ae4D","c2a85d451aa35e18":"2vsaI","402b55e92f3dbb19":"bpWUi","d50e77b23f1eafe0":"fNWan","fb6beaca94846572":"dmT5f"}],"lWtUR":[function(require,module,exports) {
module.exports = require("a262e12864897463").getBundleURL("7UhFu") + "bleu.6e21f8e8.png" + "?" + Date.now();

},{"a262e12864897463":"lgJ39"}],"lgJ39":[function(require,module,exports) {
"use strict";
var bundleURL = {};
function getBundleURLCached(id) {
    var value = bundleURL[id];
    if (!value) {
        value = getBundleURL();
        bundleURL[id] = value;
    }
    return value;
}
function getBundleURL() {
    try {
        throw new Error();
    } catch (err) {
        var matches = ("" + err.stack).match(/(https?|file|ftp|(chrome|moz|safari-web)-extension):\/\/[^)\n]+/g);
        if (matches) // The first two stack frames will be this function and getBundleURLCached.
        // Use the 3rd one, which will be a runtime in the original bundle.
        return getBaseURL(matches[2]);
    }
    return "/";
}
function getBaseURL(url) {
    return ("" + url).replace(/^((?:https?|file|ftp|(chrome|moz|safari-web)-extension):\/\/.+)\/[^/]+$/, "$1") + "/";
} // TODO: Replace uses with `new URL(url).origin` when ie11 is no longer supported.
function getOrigin(url) {
    var matches = ("" + url).match(/(https?|file|ftp|(chrome|moz|safari-web)-extension):\/\/[^/]+/);
    if (!matches) throw new Error("Origin not found");
    return matches[0];
}
exports.getBundleURL = getBundleURLCached;
exports.getBaseURL = getBaseURL;
exports.getOrigin = getOrigin;

},{}],"3gpNe":[function(require,module,exports) {
module.exports = require("49bf21a89cc9e7dc").getBundleURL("7UhFu") + "bleu_left.14ed9a62.png" + "?" + Date.now();

},{"49bf21a89cc9e7dc":"lgJ39"}],"6YS4U":[function(require,module,exports) {
module.exports = require("2bc60b016fa45b8a").getBundleURL("7UhFu") + "bleu_right.46e64f5d.png" + "?" + Date.now();

},{"2bc60b016fa45b8a":"lgJ39"}],"6ob0U":[function(require,module,exports) {
module.exports = require("b604752f2821f954").getBundleURL("7UhFu") + "dauphin.e1131626.png" + "?" + Date.now();

},{"b604752f2821f954":"lgJ39"}],"dMlsJ":[function(require,module,exports) {
module.exports = require("543dd131a3fb8d86").getBundleURL("7UhFu") + "dauphin_left.18f168f0.png" + "?" + Date.now();

},{"543dd131a3fb8d86":"lgJ39"}],"2CZKI":[function(require,module,exports) {
module.exports = require("c8599a61f72ba1c").getBundleURL("7UhFu") + "dauphin_right.fb10c6bd.png" + "?" + Date.now();

},{"c8599a61f72ba1c":"lgJ39"}],"d31qj":[function(require,module,exports) {
module.exports = require("c4b05ff4a7af58d3").getBundleURL("7UhFu") + "dragon.0e16ede8.png" + "?" + Date.now();

},{"c4b05ff4a7af58d3":"lgJ39"}],"iWtfW":[function(require,module,exports) {
module.exports = require("a8b029470a1bcf96").getBundleURL("7UhFu") + "dragon_left.7056d37d.png" + "?" + Date.now();

},{"a8b029470a1bcf96":"lgJ39"}],"1VweF":[function(require,module,exports) {
module.exports = require("cb0f3e19c8397875").getBundleURL("7UhFu") + "dragon_right.9b170cb9.png" + "?" + Date.now();

},{"cb0f3e19c8397875":"lgJ39"}],"cCexY":[function(require,module,exports) {
module.exports = require("a0c26370f3d59159").getBundleURL("7UhFu") + "ecureuil.dfcb0d43.png" + "?" + Date.now();

},{"a0c26370f3d59159":"lgJ39"}],"6oSdW":[function(require,module,exports) {
module.exports = require("ab647b5865079e83").getBundleURL("7UhFu") + "ecureuil_left.6cfe93b2.png" + "?" + Date.now();

},{"ab647b5865079e83":"lgJ39"}],"eM2ng":[function(require,module,exports) {
module.exports = require("f5982ffce7bdeffc").getBundleURL("7UhFu") + "ecureuil_right.98d2a877.png" + "?" + Date.now();

},{"f5982ffce7bdeffc":"lgJ39"}],"kriua":[function(require,module,exports) {
module.exports = require("e1947d92d9e4187a").getBundleURL("7UhFu") + "foret_papier.0c55413b.png" + "?" + Date.now();

},{"e1947d92d9e4187a":"lgJ39"}],"9OFE7":[function(require,module,exports) {
module.exports = require("cd80d598294609ce").getBundleURL("7UhFu") + "foret_papier_2.326014a0.png" + "?" + Date.now();

},{"cd80d598294609ce":"lgJ39"}],"flgNE":[function(require,module,exports) {
module.exports = require("1215118cd4f2ada2").getBundleURL("7UhFu") + "foret_papier_3.ec17351e.png" + "?" + Date.now();

},{"1215118cd4f2ada2":"lgJ39"}],"iHOnz":[function(require,module,exports) {
module.exports = require("6773be283520d84c").getBundleURL("7UhFu") + "foret_papier_4.22390e8a.png" + "?" + Date.now();

},{"6773be283520d84c":"lgJ39"}],"BVXzg":[function(require,module,exports) {
module.exports = require("7c0e400a0a3fee6").getBundleURL("7UhFu") + "grenouille.a2106ca8.png" + "?" + Date.now();

},{"7c0e400a0a3fee6":"lgJ39"}],"hvVXZ":[function(require,module,exports) {
module.exports = require("2fba4666694d1afe").getBundleURL("7UhFu") + "grenouille_left.ef634a08.png" + "?" + Date.now();

},{"2fba4666694d1afe":"lgJ39"}],"6vAUN":[function(require,module,exports) {
module.exports = require("6d8f48789eb67a22").getBundleURL("7UhFu") + "grenouille_right.a417450a.png" + "?" + Date.now();

},{"6d8f48789eb67a22":"lgJ39"}],"64e8A":[function(require,module,exports) {
module.exports = require("c6ee0378a2240903").getBundleURL("7UhFu") + "grue_2.2a273635.png" + "?" + Date.now();

},{"c6ee0378a2240903":"lgJ39"}],"c0TI7":[function(require,module,exports) {
module.exports = require("1820127f02181eec").getBundleURL("7UhFu") + "grue_2_left.6f3a4921.png" + "?" + Date.now();

},{"1820127f02181eec":"lgJ39"}],"8JA6r":[function(require,module,exports) {
module.exports = require("8f27f7b7b529b19f").getBundleURL("7UhFu") + "grue_2_right.a37c2db7.png" + "?" + Date.now();

},{"8f27f7b7b529b19f":"lgJ39"}],"7OjXt":[function(require,module,exports) {
module.exports = require("682dab430413a7d").getBundleURL("7UhFu") + "hibou.a3d1da64.png" + "?" + Date.now();

},{"682dab430413a7d":"lgJ39"}],"2Ct0k":[function(require,module,exports) {
module.exports = require("5f633f2db502d43").getBundleURL("7UhFu") + "hibou_left.29fe08d6.png" + "?" + Date.now();

},{"5f633f2db502d43":"lgJ39"}],"hf8mY":[function(require,module,exports) {
module.exports = require("61b5b4f6130c73b0").getBundleURL("7UhFu") + "hibou_right.97b689cb.png" + "?" + Date.now();

},{"61b5b4f6130c73b0":"lgJ39"}],"7pI4l":[function(require,module,exports) {
module.exports = require("75af021766f509c4").getBundleURL("7UhFu") + "hirondelle.40fb7fca.png" + "?" + Date.now();

},{"75af021766f509c4":"lgJ39"}],"kiJev":[function(require,module,exports) {
module.exports = require("c2fc33ae1b88c9f1").getBundleURL("7UhFu") + "hirondelle_left.107d0d0b.png" + "?" + Date.now();

},{"c2fc33ae1b88c9f1":"lgJ39"}],"3YY5r":[function(require,module,exports) {
module.exports = require("cf1ed4c1611562ba").getBundleURL("7UhFu") + "hirondelle_right.50f27263.png" + "?" + Date.now();

},{"cf1ed4c1611562ba":"lgJ39"}],"1YsnD":[function(require,module,exports) {
module.exports = require("27460a0bbd2f9c38").getBundleURL("7UhFu") + "maman_renard.dd0859c9.png" + "?" + Date.now();

},{"27460a0bbd2f9c38":"lgJ39"}],"6vzLg":[function(require,module,exports) {
module.exports = require("a0f96553a66b4b6e").getBundleURL("7UhFu") + "maman_renard_left.91d596a6.png" + "?" + Date.now();

},{"a0f96553a66b4b6e":"lgJ39"}],"i0jL7":[function(require,module,exports) {
module.exports = require("cd4ae7a750679b2a").getBundleURL("7UhFu") + "maman_renard_right.5c32e93a.png" + "?" + Date.now();

},{"cd4ae7a750679b2a":"lgJ39"}],"6N4el":[function(require,module,exports) {
module.exports = require("f57d128596f56121").getBundleURL("7UhFu") + "ours.28053d28.png" + "?" + Date.now();

},{"f57d128596f56121":"lgJ39"}],"7OEng":[function(require,module,exports) {
module.exports = require("839da0757ed2a2d3").getBundleURL("7UhFu") + "ours_left.a586f36a.png" + "?" + Date.now();

},{"839da0757ed2a2d3":"lgJ39"}],"aNHz5":[function(require,module,exports) {
module.exports = require("4b0d4134267ad8cc").getBundleURL("7UhFu") + "ours_right.58b7c2df.png" + "?" + Date.now();

},{"4b0d4134267ad8cc":"lgJ39"}],"3R36i":[function(require,module,exports) {
module.exports = require("285ef8496b03b2b5").getBundleURL("7UhFu") + "papillon.459cddc8.png" + "?" + Date.now();

},{"285ef8496b03b2b5":"lgJ39"}],"fCKSi":[function(require,module,exports) {
module.exports = require("b94df7bec00e2f42").getBundleURL("7UhFu") + "papillon_left.85bf8049.png" + "?" + Date.now();

},{"b94df7bec00e2f42":"lgJ39"}],"aBBz9":[function(require,module,exports) {
module.exports = require("e0c47dec21cc20c8").getBundleURL("7UhFu") + "papillon_right.3315d9e9.png" + "?" + Date.now();

},{"e0c47dec21cc20c8":"lgJ39"}],"6q0Fz":[function(require,module,exports) {
module.exports = require("8c46e947919689a7").getBundleURL("7UhFu") + "petit_renard.5a36c068.png" + "?" + Date.now();

},{"8c46e947919689a7":"lgJ39"}],"6S1Sx":[function(require,module,exports) {
module.exports = require("d8ec540a3267f6e9").getBundleURL("7UhFu") + "petit_renard_2.a3dad167.png" + "?" + Date.now();

},{"d8ec540a3267f6e9":"lgJ39"}],"aV5t8":[function(require,module,exports) {
module.exports = require("cf3825cae1b2f3ab").getBundleURL("7UhFu") + "petit_renard_2_left.8d3c80ba.png" + "?" + Date.now();

},{"cf3825cae1b2f3ab":"lgJ39"}],"123Ca":[function(require,module,exports) {
module.exports = require("f9103207befa32a3").getBundleURL("7UhFu") + "petit_renard_2_right.6fc35e33.png" + "?" + Date.now();

},{"f9103207befa32a3":"lgJ39"}],"aPrl7":[function(require,module,exports) {
module.exports = require("95aa1b433477b647").getBundleURL("7UhFu") + "poisson.2f3dcb01.png" + "?" + Date.now();

},{"95aa1b433477b647":"lgJ39"}],"hcDPQ":[function(require,module,exports) {
module.exports = require("f2b2d4b2085d9598").getBundleURL("7UhFu") + "poisson_left.11a205a4.png" + "?" + Date.now();

},{"f2b2d4b2085d9598":"lgJ39"}],"9u79o":[function(require,module,exports) {
module.exports = require("b2b50ad683263ebb").getBundleURL("7UhFu") + "poisson_right.ccbdaf67.png" + "?" + Date.now();

},{"b2b50ad683263ebb":"lgJ39"}],"jW09L":[function(require,module,exports) {
module.exports = require("650baec0c1446cda").getBundleURL("7UhFu") + "poupees.0398132c.png" + "?" + Date.now();

},{"650baec0c1446cda":"lgJ39"}],"bBXfb":[function(require,module,exports) {
module.exports = require("7524b655d82b11e2").getBundleURL("7UhFu") + "renard.7b23905b.png" + "?" + Date.now();

},{"7524b655d82b11e2":"lgJ39"}],"1CoXX":[function(require,module,exports) {
module.exports = require("8b6ecdfccac42d86").getBundleURL("7UhFu") + "requin.019bd6b4.png" + "?" + Date.now();

},{"8b6ecdfccac42d86":"lgJ39"}],"iMPCt":[function(require,module,exports) {
module.exports = require("5056b6e2e3187ee4").getBundleURL("7UhFu") + "requin_left.2c69f8d7.png" + "?" + Date.now();

},{"5056b6e2e3187ee4":"lgJ39"}],"dvRno":[function(require,module,exports) {
module.exports = require("9a435208b9fc0c65").getBundleURL("7UhFu") + "requin_right.3effefde.png" + "?" + Date.now();

},{"9a435208b9fc0c65":"lgJ39"}],"4xBT2":[function(require,module,exports) {
module.exports = require("8bf456e1b97dd1f4").getBundleURL("7UhFu") + "rose_cropped_left.397f95c5.png" + "?" + Date.now();

},{"8bf456e1b97dd1f4":"lgJ39"}],"e782M":[function(require,module,exports) {
module.exports = require("4c001aacb5665621").getBundleURL("7UhFu") + "rose_cropped_right.fcfa6694.png" + "?" + Date.now();

},{"4c001aacb5665621":"lgJ39"}],"7ae4D":[function(require,module,exports) {
module.exports = require("778a415c77cbade4").getBundleURL("7UhFu") + "rose_left.42b8223f.png" + "?" + Date.now();

},{"778a415c77cbade4":"lgJ39"}],"2vsaI":[function(require,module,exports) {
module.exports = require("72efd3fe22a37193").getBundleURL("7UhFu") + "rose_right.3275e6da.png" + "?" + Date.now();

},{"72efd3fe22a37193":"lgJ39"}],"bpWUi":[function(require,module,exports) {
module.exports = require("dcc828276a2a3a31").getBundleURL("7UhFu") + "sanglier.ab4ded8d.png" + "?" + Date.now();

},{"dcc828276a2a3a31":"lgJ39"}],"fNWan":[function(require,module,exports) {
module.exports = require("2326778293acbaaf").getBundleURL("7UhFu") + "sanglier_left.94f7ccf7.png" + "?" + Date.now();

},{"2326778293acbaaf":"lgJ39"}],"dmT5f":[function(require,module,exports) {
module.exports = require("f2422937a2ea1fb7").getBundleURL("7UhFu") + "sanglier_right.84addd0f.png" + "?" + Date.now();

},{"f2422937a2ea1fb7":"lgJ39"}]},["iJYvl","h7u1C"], "h7u1C", "parcelRequire86ed")

//# sourceMappingURL=index.b71e74eb.js.map
